import { Component, OnInit } from "@angular/core";
import { WpPagesService } from "../services/wp-pages.service";

@Component({
  selector: "#slider",
  templateUrl: "./sliders.component.html",
  styleUrls: ["./sliders.component.css"],
  host: {
    class: "sl-slider-wrapper",
  },
})
export class SlidersComponent implements OnInit {
  constructor(private wp: WpPagesService) {
    this.get();
  }
  pages: any;
  length;
  get() {
    this.wp.get().subscribe(
      (res) => {
        this.pages = res;
        this.length = this.pages.length;
      },
      (err) => console.log(err)
    );
  }

  ngOnInit(): void {}
}
