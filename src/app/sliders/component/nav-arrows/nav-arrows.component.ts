import { Component, OnInit } from "@angular/core";

@Component({
  selector: "#nav-arrows",
  templateUrl: "./nav-arrows.component.html",
  styleUrls: ["./nav-arrows.component.css"],
  host: {
    class: "nav-arrows",
  },
})
export class NavArrowsComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
