import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "#nav-multi-square",
  templateUrl: "./nav-multi-square.component.html",
  styleUrls: ["./nav-multi-square.component.css"],
  host: {
    class: "nav-multi-square nav-intro opacity-0",
  },
})
export class NavMultiSquareComponent implements OnInit {
  constructor() {}
  @Input() pages: any;
  @Input() data: number = 1;
  setWidth() {
    return 100 / this.data;
  }

  ngOnInit(): void {}
}
