import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "#sl-slider",
  templateUrl: "./sl-slider.component.html",
  styleUrls: ["./sl-slider.component.css"],
  host: {
    class: "sl-slider",
  },
})
export class SlSliderComponent implements OnInit {
  constructor() {}
  @Input() pages: any;

  setClass(i) {
    let dir;
    i = i + 1;
    if (i % 2 == 1) {
      dir = "sl-slide-horizontal";
    } else {
      dir = "sl-slide-vertical";
    }
    return "bg-" + i + " " + dir;
  }
  orientacion(i) {
    if (i) {
      return "vertical";
    } else {
      return "horizontal";
    }
  }
  setBackground(url) {
    console.log("hola");
  }
  ngOnInit(): void {}
}
