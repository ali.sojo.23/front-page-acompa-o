import { Component, OnInit } from "@angular/core";

@Component({
  selector: "#loading",
  templateUrl: "./loading.component.html",
  styleUrls: ["./loading.component.css"],
  host: {
    class: "dark-back",
  },
})
export class LoadingComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
