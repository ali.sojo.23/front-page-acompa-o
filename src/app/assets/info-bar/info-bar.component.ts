import { Component, OnInit } from "@angular/core";

@Component({
  selector: "#info-bar",
  templateUrl: "./info-bar.component.html",
  styleUrls: ["./info-bar.component.css"],
  host: {
    class: "info-bar bar-intro opacity-0",
  },
})
export class InfoBarComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
