import { Component } from "@angular/core";

@Component({
  selector: "body",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent {
  constructor() {
    this.config();
  }
  title = "home";

  config() {
    let lang: string = window.navigator.language.substr(0, 2);
    if (lang == "pt") {
      if (window.location.href != "https://acompanho.com.br") {
        window.location.href = "https://acompanho.com.br";
      }
    }
  }
}
