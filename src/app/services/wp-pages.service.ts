import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
@Injectable({
  providedIn: "root",
})
export class WpPagesService {
  constructor(private http: HttpClient) {}
  get(): Observable<any> {
    return this.http.get(
      "https://webfront.incgoin.com/xn--acompao-9za/wp-json/wp/v2/pages?_embed&filter[orderby]=date&order=asc"
    );
  }
}
