import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LoadingComponent } from "./assets/loading/loading.component";
import { InfoBarComponent } from "./assets/info-bar/info-bar.component";
import { SlidersComponent } from "./sliders/sliders.component";
import { SlSliderComponent } from "./sliders/component/sl-slider/sl-slider.component";
import { NavArrowsComponent } from "./sliders/component/nav-arrows/nav-arrows.component";
import { NavMultiSquareComponent } from "./sliders/component/nav-multi-square/nav-multi-square.component";
import { WpPagesService } from "./services/wp-pages.service";
import { HttpClientModule } from "@angular/common/http";
import { SafeHtmlPipe } from './pipe/safe-html.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LoadingComponent,
    InfoBarComponent,
    SlidersComponent,
    SlSliderComponent,
    NavArrowsComponent,
    NavMultiSquareComponent,
    SafeHtmlPipe,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule],
  providers: [WpPagesService],
  bootstrap: [AppComponent],
})
export class AppModule {}
